use std::collections::HashMap;

use crate::constants::status;
use crate::constants::headers;
use crate::constants::content_types;

pub const DATA_BOUNDARY: &[u8] = b"==================DATA_BOUDARY==================";

#[derive(Debug, Clone)]
pub struct Handler {
    pub method: String,
    pub resource_path: String,
    pub data: Vec<u8>,
    pub status: String,
    pub status_message: String,
    pub headers: HashMap<String, String>
}

impl Handler {

    pub fn new() -> Self {
        let mut headers = HashMap::new();
        headers.insert(headers::CONTENT_TYPE.to_string(), "text/plain".to_string());
        Handler {
            method: String::from(""),
            resource_path: String::from(""),
            data: Vec::new(),
            status: String::from(status::OK),
            status_message: String::from(""),
            headers: headers
        }
    }

    pub fn ok(&self) -> bool {
        self.status == status::OK
    }

    pub fn data_str(&self) -> String {
        let txt = String::from_utf8_lossy(&self.data[..]);
        txt.to_string()
    }

    pub fn get_data(&self) -> String {
        let default = content_types::PLAIN_TEXT.to_string();
        let content_type = self.headers.get(headers::CONTENT_TYPE)
            .unwrap_or(&default);
        match &content_type[..] {
            content_types::PLAIN_TEXT => self.data_str(),
            _ => self.data_str(),
        }
    }

    pub fn copy_header(&mut self, key: String, other: &Handler) {
        if let Some(header) = other.headers.get(&key) {
            self.headers.insert(key, header.to_string());
        }
    }

    pub fn format_header(&self, key: String) -> Option<String> {
        if let Some(header) = self.headers.get(&key) {
            Some(format!("{}: {}", key, header))
        } else {
            None
        }
    }

    pub fn get_formatted_headers(&self) -> Vec<u8> {
        self.headers.keys()
            .flat_map(|k| self.format_header(k.to_string()))
            .flat_map(|val| (val + "\n").into_bytes())
            .collect()
    }


    pub fn parse_header(&self, header: &String) -> Option<(String, String)> {
        let vals: Vec<&str> = header.splitn(2, ':').collect();
        if vals.len() != 2 {
            Some((String::from(vals[0]), String::from(vals[1])))
        } else {
            None
        }

    }

    pub fn set_header(&mut self, key: String, val: String) {
        self.headers.insert(key, val);
    }

    pub fn set_status(&mut self, status: &status::Status) {
        self.status = status.value().to_string();
    }

    pub fn get_header(&self, key: String) -> Option<&String> {
        self.headers.get(&key)
    }

    pub fn set_data(&mut self, data: Vec<u8>) {
        self.data = data;
    }
}

