use std::io::{self, Write};
use std::net::{TcpStream};

use super::handler::{Handler, DATA_BOUNDARY};

pub struct  Serializer {
    pub handler: Handler,
}


impl  Serializer {
    pub fn new(request_handler_: &Handler) -> Self {
        let mut handler_  = Handler::new();
        let request_handler = request_handler_.clone();
        handler_.resource_path = request_handler.resource_path;
        handler_.method = request_handler.method;
         Serializer {
            handler: handler_,
        }
    }

    fn to_bytes(&self) -> Vec<u8> {
        let formatted_headers = self.handler.get_formatted_headers();
        let mut res: Vec<u8> = Vec::new();
        res.extend(format!("{} {}\n", self.handler.method, self.handler.resource_path).as_bytes());
        res.extend(format!("{} {}\n", self.handler.status, self.handler.status_message).as_bytes());
        res.extend(formatted_headers);
        res.extend(DATA_BOUNDARY);
        res.extend(self.handler.data.as_slice());
        res
    }

    pub fn send(mut self, stream: &mut TcpStream, data: Vec<u8>) -> io::Result<()> {
        self.handler.set_data(data);
        stream.write_all(self.to_bytes().as_slice())
    }
}