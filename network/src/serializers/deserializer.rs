use std::io::{self, Read};
use std::net::{TcpStream};

use super::handler::{Handler,DATA_BOUNDARY};
use crate::constants::status;

pub struct  Deserializer {
    pub handler: Handler,
    pub client_host: String,
}

pub const BUFFER_SIZE : usize = 512;

fn match_sub_vector(subset: &[u8], delim: &[u8]) -> bool {
    let mut delimeter = delim.to_vec();
    delimeter.reverse();
    for el in subset.iter() {
        if let Some(d) = delimeter.pop() {
            if el != &d {
                return false
            }
        }
    }
    return true
}

fn get_index(data: &Vec<u8>, delim: &[u8]) -> Option<usize> {
    for (i, _el) in data.iter().enumerate() {
        if match_sub_vector(data.split_at(i).1, delim) {
            return Some(i)
        }
    }
    return None
}

impl  Deserializer {

    pub fn invalid(host: String, message: &str) ->  Deserializer {
        let mut local_handler = Handler::new();
        local_handler.status = status::BAD_REQUEST.to_string();
        local_handler.status_message = format!("Invalid  Deserializer: {}", message);
         Deserializer {
            handler: local_handler,
            client_host: host,
        }
    }
    
    pub fn empty(host: String) ->  Deserializer {
        let mut local_handler = Handler::new();
        local_handler.status = status::NOT_FOUND.to_string();
        local_handler.status_message = format!("Could not receive a response from {}", host);
         Deserializer {
            client_host: host,
            handler: local_handler,
        }
    }

    pub fn new(host: String, request_data: Vec<u8>) ->  Deserializer {
        let mut local_handler = Handler::new();
        let len_boundary = DATA_BOUNDARY.len();
        let boundary_pos = get_index(&request_data, DATA_BOUNDARY).unwrap_or(0);

        if boundary_pos == 0 {
            return  Deserializer::invalid(host, "Data not found")
        }
        
        let (request_info, _data) = request_data.split_at(boundary_pos);
        let data = _data.split_at(len_boundary).1;
        local_handler.set_data(data.to_vec());
        let request_info_str = String::from_utf8_lossy(request_info);
        let mut request_vec = request_info_str.split("\n");
        let path_and_method = match request_vec.next() {
            Some(txt) => txt.to_string(),
            None => return  Deserializer::invalid(host, " Deserializer path and method not found")
        };
        let status_text =  match request_vec.next() {
            Some(txt) => txt.to_string(),
            None => return  Deserializer::invalid(host, " Deserializer status found")
        };
        let headers = request_vec;
        let status_: Vec<&str> = status_text.splitn(2, ' ').collect();
        local_handler.status = status_[0].to_string();
        local_handler.status_message = status_[1].to_string();
        let req_info: Vec<&str> = path_and_method.splitn(2, ' ').collect();
        local_handler.method = req_info[0].to_string();
        local_handler.resource_path = req_info[1].to_string();
        for header in headers {
            if header.trim() != "" {
                let h: Vec<&str> = header.splitn(2, ": ").collect();
                local_handler.set_header(h[0].to_string(), h[1].to_string());
            }
        }
        

         Deserializer {
            handler: local_handler,
            client_host: host, 
        }
    }

    pub fn recv(stream: &mut TcpStream, host: String) -> io::Result<Self> {
        let mut data = Vec::new();
        let mut buffer = [0; BUFFER_SIZE];
        loop {
            if let Ok(n) = stream.read(&mut buffer) {
                data.extend_from_slice(&buffer[..n]);
                if n < BUFFER_SIZE {
                    break
                }
            }
        }
        if buffer.len() > 0 {
            Ok( Deserializer::new(host, data))
        } else {
            Ok( Deserializer::empty(host))
        }
    }
}

