use std::collections::HashMap;
use std::rc::Rc;
use crate::serializers::context::{Context, Handler};
use crate::constants::{status};



pub type ResponseType = String;
pub type MiddlewareClosure = Box<Fn(&mut Context) -> Box<ResponseT>>;
pub type Middleware = fn(MiddlewareClosure) -> MiddlewareClosure;

pub trait ResponseT {
    fn respond(&self, ctx: &mut Context) -> ResponseType;
}

impl ResponseT for (status::Status, &str) {
    fn respond(&self, ctx: &mut Context) -> ResponseType {
        let (status, data) = self;
        ctx.res.handler.set_status(status);
        data.to_string()
    }
}

impl ResponseT for (status::Status, String) {
    fn respond(&self, ctx: &mut Context) -> ResponseType {
        let (status, data) = self;
        ctx.res.handler.set_status(status);
        data.to_string()
    }
}

#[derive(Eq, PartialEq, Hash)]
pub enum Method {
    GET,
    POST,
    UNKNOWN
}

impl Method {
    pub fn from_str(txt: String) -> Self {
        match &txt.to_uppercase()[..] {
            "GET" => Method::GET,
            "POST" => Method::POST,
            _ => Method::UNKNOWN
        }
    }
}

pub type RouteKey = (Method, String);
pub struct Router {
    pub routes: HashMap<RouteKey, Rc<MiddlewareClosure>>,
    root_path: String,
    middlewares: Vec<Middleware>,
}

fn not_found_handler(_ctx: &mut Context) -> impl ResponseT {
    (status::Status::NotFound, "Route not found")
}

fn bad_request_handler(_ctx: &mut Context) -> impl ResponseT {
    (status::Status::InvalidRequest, "Bad Request")
}

impl Router {
    pub fn new(root_path: &str) -> Self {
        let map = HashMap::new();
        Router {
            routes: map,
            root_path: root_path.to_string(),
            middlewares: Vec::new(),
        }
    }


    fn wrap_route_with_middleware<T: 'static>(&self, route: fn(&mut Context) -> T) 
        -> MiddlewareClosure 
        where
            T: ResponseT
    {
        Box::new(move |ctx| {
            Box::new(route(ctx))
        })
    }

    pub fn get_route<'a>(&self, req_handler: &Handler) -> Rc<MiddlewareClosure> {
        if !req_handler.ok() {
            return Rc::new(self.wrap_route_with_middleware(bad_request_handler))
        }
        let request_handler = req_handler.clone();
        let method = Method::from_str(request_handler.method);
        let key = (method, request_handler.resource_path);
        match self.routes.get(&key) {
            Some(handler) => {
                let res = handler.clone();
                res
            },
            None => Rc::new(self.wrap_route_with_middleware(not_found_handler))
        }
    }

    pub fn middleware(&mut self, handler: Middleware) -> &mut Self {
        self.middlewares.push(handler);
        self
    }
    
    fn insert_route(&mut self, method: Method, resource_path: &str, final_handler: MiddlewareClosure) {
        self.routes.insert((method, resource_path.to_string()), Rc::new(final_handler));
    }

    fn _route<T: 'static>(&mut self, method: Method, resource_path: &str, handler: fn(&mut Context) -> T) -> &mut Self 
        where
            T: ResponseT
    {
        let final_handler = self.wrap_route_with_middleware(handler);
        let final_handler = self.apply_middlewares(final_handler);
        self.insert_route(method, &format!("{}{}", self.root_path, resource_path), final_handler);
        self
    }

    pub fn get<T: 'static>(&mut self, resource_path: &str, handler: fn(&mut Context) -> T) -> &mut Self 
        where
            T: ResponseT
    {
        self._route(Method::GET, resource_path, handler)
    }
    
    pub fn post<T: 'static>(&mut self, resource_path: &str, handler: fn(&mut Context) -> T) -> &mut Self 
        where
            T: ResponseT
    {
        self._route(Method::POST, resource_path, handler)
    }

    fn apply_middlewares(&mut self, handler: MiddlewareClosure) -> MiddlewareClosure {
        self.middlewares.iter().rev()
            .fold(handler, |acc, func| func(acc) )
    }

    pub fn done(&mut self) -> &mut Self {
        self.middlewares.clear();
        self
    }

    pub fn resource<F>(&mut self, resource_path: &str, make_route: F) -> &mut Self
        where 
            F: Fn(&mut Router) -> ()
    {
        let mut router = Router::new(&format!("{}{}", self.root_path, resource_path));
        make_route(&mut router);
        for ((method, path), handler) in router.routes.into_iter() {
            if let Ok(h) = Rc::try_unwrap(handler) {
                let final_middleware = self.apply_middlewares(h);
                self.insert_route(method, &path, final_middleware);
            }
        }
        self
    }
}