use std::io;
use std::error::Error;
use std::net::{TcpListener, TcpStream};
// use std::thread;
// use std::path::Path;
// use std::fs::File;
// use uuid::Uuid;

use super::mixins::routing::{Router};
use crate::serializers::context::{Context, Deserializer,  Serializer};
// fn get_server_id() -> Uuid {
//     let path = Path::new("server_id.txt");
//     let display = path.display();
//     let file = File::open(path)
//         .expect(format!("File {} could not be opened", display));
//     let mut buffer = String::new();
//     file.read_to_string(&mut buffer).expect("Could not read file contents");
//     Uuid::new_v4().to_hyphenated_string()
//     Uuid::parse_str()
// }

pub struct Server {
    pub router: Router
}

impl Server {
    pub fn new<F>(add_routes: F) -> Self 
    where
        F: Fn(&mut Router) -> () {
        let mut router = Router::new("");
        add_routes(&mut router);
        router.done();
        Server {
            router: router
        }
    }

    fn handle_client(&self, mut stream: TcpStream) {
        let host = stream.peer_addr().map(| addr | addr.to_string()).unwrap();
        match Deserializer::recv(&mut stream, host){
            Ok(request) => {
                let handler = self.router.get_route(&request.handler);
                let response =  Serializer::new(&request.handler);
                let mut ctx = Context {
                    req: request,
                    res: response
                };
                let data = handler(&mut ctx);
                let byte_response = data.respond(&mut ctx).as_bytes().to_vec();
                ctx.res.send(&mut stream, byte_response).unwrap();
            },
            Err(ref et) => {
                panic!("Error {}", et.description());
            },
        }
    }

    pub fn start(&self, host: &str) -> io::Result<()> {
        let listener = TcpListener::bind(host.to_string())
            .expect(&format!("Failed to bind to {}", host));
        println!("Server running at {}", host);
        for stream in listener.incoming() {
            // thread::spawn(move || {
                self.handle_client(stream.unwrap());
            // });
        }
        Ok(())
    }
}