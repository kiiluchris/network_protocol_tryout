extern crate uuid;

pub mod client;
pub mod constants;
pub mod database;
pub mod serializers;
pub mod server;
pub mod utils;

pub use server::mixins::routing::{Method, ResponseType, ResponseT};


#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
