pub fn local_hostname() -> String {
    if cfg!(windows) {
        String::from("localhost")
    } else {
        String::from("0.0.0.0")
    }  
}


pub type Addr = (String, i32);

pub fn addr_to_url(addr: &Addr) -> String {
    format!("{}:{}", addr.0, addr.1)
}

pub fn url_to_addr(url: String) -> Option<Addr> {
    let vals: Vec<&str> = url.splitn(2, ':').collect();
    if vals.len() != 2 {
        let port = vals[1].parse::<i32>();
        Some((String::from(vals[0]), port.unwrap_or(8000)))
    } else {
        None
    }
}

