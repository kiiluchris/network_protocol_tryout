use std::io;
use std::net::TcpStream;

use crate::serializers::handler::Handler;
pub use crate::serializers::context::*;

pub struct Client {
    host: String
}

impl Client {
    pub fn new(host: String) -> Self {
        Client {
            host: host
        }
    }

    fn make_request(&self, data: String, handler: Handler) -> io::Result<Deserializer> {
        let mut stream = TcpStream::connect(self.host.to_string())?;
        let req = Serializer::new(&handler);
        req.send(&mut stream, data.as_bytes().to_vec())?;
        Deserializer::recv(&mut stream, self.host.to_string())
    }

    pub fn get(&self, url: &str, data: String, mut handler: Handler) -> io::Result<Deserializer> {
        handler.resource_path = url.to_string();
        handler.method = "GET".to_uppercase();
        self.make_request(data, handler)
    }

    pub fn post(&self,  url: &str, data: String, mut handler: Handler) -> io::Result<Deserializer> {
        handler.resource_path = url.to_string();
        handler.method = "POST".to_uppercase();
        self.make_request(data, handler)
    }
}