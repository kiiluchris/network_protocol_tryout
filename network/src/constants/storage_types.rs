pub const STORE_AND_FORWARD: &str = "Store and Forward";
pub const OSD: &str = "Object Storage";
pub const UNSORTED_AGENTS: &str = "Unsorted Mobile Agents (Map Reduce)";
pub const SORTED_AGENTS: &str = "Sorted Mobile Agents (Map Reduce)";
pub const UNSORTED_AGENTS_WITH_DMC: &str = "Unsorted Mobile Agents with Domain Controller Centralized (Map Reduce)";
pub const SORTED_AGENTS_WITH_DMC: &str = "Sorted Mobile Agents with Domain Controller Centralized (Map Reduce)";
pub const UNSORTED_AGENTS_WITH_DMC_MAP_RED: &str = "Unsorted Mobile Agents with Domain Controller Child DMCs (Map Reduce)";
pub const SORTED_AGENTS_WITH_DMC_MAP_RED: &str = "Sorted Mobile Agents with Domain Controller Child DMCs (Map Reduce)";


pub const ALL_WITH_SORTING: (&str, &str, &str, &str, &str, &str, &str, &str) = (
    STORE_AND_FORWARD,
    OSD,
    UNSORTED_AGENTS,
    SORTED_AGENTS,
    UNSORTED_AGENTS_WITH_DMC,
    SORTED_AGENTS_WITH_DMC,
    UNSORTED_AGENTS_WITH_DMC_MAP_RED,
    SORTED_AGENTS_WITH_DMC_MAP_RED
);