pub const OK: &str = "OK";
pub const ERROR: &str = "ERROR";
pub const NOT_FOUND: &str = "NOT_FOUND";
pub const NO_AUTH: &str = "NO_AUTH";
pub const INVALID_AUTH: &str = "INVALID_AUTH";
pub const BAD_REQUEST: &str = "BAD_REQUEST";

pub enum Status {
    Ok,
    Error,
    NotFound,
    NoAuth,
    InvalidAuth,
    InvalidRequest,
}

impl Status {
    pub fn value(&self) -> &str {
        match self {
            Status::Ok => OK,
            Status::Error => ERROR,
            Status::NotFound => NOT_FOUND,
            Status::NoAuth => NO_AUTH,
            Status::InvalidAuth => INVALID_AUTH,
            Status::InvalidRequest => BAD_REQUEST,
        }
    }
}